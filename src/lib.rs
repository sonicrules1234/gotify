//resp = requests.post('http://192.168.0.56:4002/message?token=A2yqiAcA.8zSMwQ', json={
//        "message": message,
//        "priority": priority,
//        "title": title
//    })
use directories::BaseDirs;

pub fn send_notification(title: &str, message: &str, priority: u8) -> bool {
    let basedirectories = BaseDirs::new().unwrap();
    let homedir = basedirectories.home_dir();
    let address = std::fs::read_to_string(homedir.join(".sonicgotify")).unwrap();
    let addresstrimmed = address.trim().to_string();
    match ureq::post(&addresstrimmed)
        .send_json(ureq::json!({
            "message": message,
            "priority": priority,
            "title": title
        })) {
            Ok(_) => true,
            Err(_) => false,
        }
        //.send().expect("Unable to send notification");
}
pub fn send_notification_to(recipient: &str, title: &str, message: &str, priority: u8) -> bool {
    let addresstrimmed = recipient;
    match ureq::post(&addresstrimmed)
        .send_json(ureq::json!({
            "message": message,
            "priority": priority,
            "title": title
        })) {
            Ok(_) => true,
            Err(_) => false,
        }
        //.send().expect("Unable to send notification");
}